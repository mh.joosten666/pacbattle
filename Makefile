.PHONY: env
env:
	virtualenv .venv -p /usr/bin/python3
	. .venv/bin/activate && python -m pip install -r requirements.txt

.PHONY: run
run:
	. .venv/bin/activate && python core.py ./assets/levels/level_for_2_players.tmx "Player 1" "Player 2"
