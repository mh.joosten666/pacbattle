# PacBattle
An attempt to create a game vaguely resembling PacWars (a networking game from the days of Novell networks)
It is a platform/top down shooter/coin catching game in which multiple players in the form of a PacMan must eat coins and avoid being
killed by other players and avoid certain obstacles in a level.

### In this version (v0.1)
- Two players local game (which means 2 players at one keyboard)
- One level

### Not in this version (yet):
- Networking support


## System requirements
- A PC with an OpenGL capable video card (or a virtual machine with 3D acceleration enabled)
- A reasonable modern Linux distribution (it could work on Windows, but haven't tested it)
- Python 3.7+ and tools like `pip`
- `Git`
- `Make` (for the quick start and development purposes, present in most Linux distributions)

## Running the game
### Quick start
- Clone this git repository to some directory
- Go to this directory
- Execute `make env` (this is a one time action only to create a Python virtual environment with the necessary dependencies)
- Execute `make run` and a game will be started with players `Player 1` and `Player 2` and the included level


### More detailed start
- Activate the virtual environment by executing `. .venv/bin/activate` from your game dir
- Run the game with `python core.py [options]` (see below for explanation of the command line options)

### Command line options
`core.py [-h] [--loglevel {DEBUG,INFO,WARNING,ERROR,CRITICAL}] level_path player_names [player_names ...]`

`level_path`:            Full path to a level file. (only one is provided in `assets/levels/level_for_2_players.tmx`)

`player_names`:          A list of player names (only 2 players are supported now)

`-h, --help`:             Shows a help text

`--loglevel`:             Sets log level (useful for debugging). Default: INFO



## Levels
Levels are in .TMX format and can be designed with tools like the excellent [Tiled level editor](https://www.mapeditor.org/)
Levels should conform to the following:
- Orientiation: orthogonal
- Tile size: 50x50
- Tile layer format: Base64 (zlib compressed)
- Level size: 24x13 (wxh)
- Layers:
  - 'wall': The tiles that represents walls, players can't move through these tiles
  - 'fire': The tiles that when a player touches them, he/she is damaged 
  - 'player_home_x': The area which is considered 'home base' for player x (x is a number >=1, that also determine for how many players this level is, currently only levels for 2 players are supported in the game)
  - The actual images for the tiles can be anything as long as they are 50x50 pixels
  - You can add an optional 'name' property to the map which is displayed in the game. If it is not present the filename is displayed


  See `assets/levels/level_for_2_players.tmx` for an example



## Development
PacBattle has been developed in Python with the nice [Python Arcade Library](https://arcade.academy/)
