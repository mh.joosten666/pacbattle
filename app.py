from typing import List, Dict

import arcade
from arcade import Window

from constants import WINDOW_WIDTH, WINDOW_HEIGHT, SCREEN_TITLE
from level import Level
from views.game_view import GameView
from views.instruction_view import IntroView
from views.score_view import ScoreView


class App:

    def __init__(self, level: Level, players: List[Dict]):
        self.window = Window(WINDOW_WIDTH, WINDOW_HEIGHT, SCREEN_TITLE)
        self._intro_view = IntroView(players=players, level=level)
        self._intro_view.on_key_pressed = self._intro_view_closed
        self._game_view = GameView(level=level, players=players)
        self._game_view.on_game_ended = self._game_ended

    def _show_intro(self):
        self.window.show_view(self._intro_view)

    def run(self):
        self._show_intro()
        arcade.run()

    def _start_game(self):
        self.window.show_view(self._game_view)
        self._game_view.setup()

    def _game_ended(self, score):
        view = ScoreView(score)
        view.on_key_pressed = self._score_view_closed
        self.window.show_view(view)

    def _score_view_closed(self, key: int, modifiers: int):
        if key == arcade.key.SPACE:
            self._start_game()
        elif key == arcade.key.Q:
            exit(0)

    def _intro_view_closed(self, key: int, modifiers: int):
        self._start_game()
