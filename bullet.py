import logging
from typing import Optional, List

from arcade import Sprite, play_sound, SpriteList, check_for_collision_with_list

from constants import SPRITE_SCALING_LASER, NORTH, SOUTH, EAST, WEST, BULLET_SPEED, GAME_SOUNDS, GAME_SOUND_VOLUME, \
    SCREEN_HEIGHT, SCREEN_WIDTH
from player import Player

logger = logging.getLogger(__name__)

class Bullet(Sprite):
    def __init__(self):
        super().__init__(":resources:images/space_shooter/laserBlue01.png", SPRITE_SCALING_LASER)
        self.origin_player: Optional[Player] = None

    def fire(self, origin_player: Player):
        self.origin_player = origin_player
        if self.origin_player.get_direction() == NORTH:
            self.angle = 90
            # Position the self
            self.center_x = self.origin_player.center_x
            self.bottom = self.origin_player.center_y + self.origin_player.height / 2
        elif self.origin_player.get_direction() == SOUTH:
            self.angle = 90
            # Position the self
            self.center_x = self.origin_player.center_x
            self.top = self.origin_player.center_y - self.origin_player.height / 2
        elif self.origin_player.get_direction() == EAST:
            # Position the self
            self.center_y = self.origin_player.center_y
            self.left = self.origin_player.center_x + self.origin_player.width / 2
        elif self.origin_player.get_direction() == WEST:
            # Position the self
            self.center_y = self.origin_player.center_y
            self.right = self.origin_player.center_x - self.origin_player.width / 2

        self.change_x = self.origin_player.get_direction()[0] * BULLET_SPEED
        self.change_y = self.origin_player.get_direction()[1] * BULLET_SPEED

        logger.info(f"{self.origin_player.get_player_data()['name']} fired a bullet")
        play_sound(GAME_SOUNDS['pew'], volume=GAME_SOUND_VOLUME)

    def check_target_hit(self, player_list: SpriteList, wall_list: SpriteList, do_not_touch_list: SpriteList):

        wall_collisions: List[Sprite] = check_for_collision_with_list(self, wall_list)
        dnt_collisions: List[Sprite] = check_for_collision_with_list(self, do_not_touch_list)
        player_collisions: List[Sprite] = check_for_collision_with_list(self, player_list)

        # Bullet -> Wall
        if wall_collisions:
            logger.info(f"{self.origin_player.get_player_data()['name']}'s bullet hit a wall")
            play_sound(GAME_SOUNDS['hit_wall'], volume=GAME_SOUND_VOLUME)
            self.kill()

        # Bullet -> Do not touch walls        
        elif dnt_collisions:
            logger.debug(f"{self.origin_player.get_player_data()['name']}'s bullet hit a do not touch objects")
            play_sound(GAME_SOUNDS['hit_wall'], volume=GAME_SOUND_VOLUME)
            self.kill()

        # Bullet -> Players        
        elif player_collisions:
            self.kill()
            for player in player_collisions:
                p: Player = player
                p.check_hit_by_bullet(self)

        elif self._is_off_screen():
            # Off screen
            logger.debug(f"Off screen")
            self.kill()

    def _is_off_screen(self) -> bool:
        return self.top < 0 or self.bottom > SCREEN_HEIGHT or self.left < 0 or self.right > SCREEN_WIDTH
