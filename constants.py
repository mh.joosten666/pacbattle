import arcade

WINDOW_HEIGHT = 750
WINDOW_WIDTH = 1200
SCREEN_WIDTH = WINDOW_WIDTH
SCREEN_HEIGHT = WINDOW_HEIGHT - 100
SCREEN_TITLE = 'PacBattle'
PLAYER_SIZE = (20, 20)
COIN_SCALING = 0.4
SPRITE_SCALING_LASER = 0.8
NUMBER_OF_LIVES = 4
PLAYER_SCALE = 0.6
# Direction vectors
NORTH = (0, 1)
SOUTH = (0, -1)
EAST = (1, 0)
WEST = (-1, 0)

PLAYER_SPEED = 5
BULLET_SPEED = PLAYER_SPEED + 5
TILE_SCALING = 1

GAME_SOUNDS = {'coin_collect': arcade.load_sound(":resources:sounds/coin1.wav"),
               'ouch': arcade.load_sound(":resources:sounds/hurt3.wav"),
               'pew': arcade.load_sound(":resources:sounds/laser2.wav"),
               'hit_wall': arcade.load_sound(":resources:sounds/hit3.wav"),
               'player_destroyed': arcade.load_sound(":resources:sounds/explosion1.wav"),
               'player_dead': arcade.load_sound(":resources:sounds/gameover1.wav"),
               'player_healthy': arcade.load_sound(":resources:sounds/upgrade1.wav")}
GAME_SOUND_VOLUME = 0.1
MUSIC_VOLUME = 30

DO_NOT_TOUCH_DAMAGE = 3
SELF_BULLET_HIT_DAMAGE = 4
BULLET_HIT_DAMAGE = 8
COIN_SCORE = 1
KILL_BONUS = 5

WALLS = 'wall'
DO_NOT_TOUCH_OBJECTS = 'fire'
PLAYER_HOME_AREA_LAYER_PREFIX = 'player_home'

