import argparse
import logging

import arcade

from app import App
from level import Level

logger = logging.getLogger(__name__)


def setup_logging(loglevel):
    loglevel = getattr(logging, loglevel)
    logging.basicConfig(level=loglevel, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def main():
    """ Main method """
    parser = argparse.ArgumentParser(
        description="PacBattle v0.1")
    parser.add_argument("level_path", help="Full path to a level file (*.tmx)")
    parser.add_argument("player_names", nargs='+', help="A list of player names")
    parser.add_argument("--loglevel", help="Minimum loglevel",
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], default="INFO")

    args = parser.parse_args()
    setup_logging(args.loglevel)
    level = Level.from_tmx(tmx_file=args.level_path, music_file='assets/game_music.mp3')

    if len(args.player_names) != level.number_of_player_home_areas():
        logger.fatal(f"The level is suitable for {level.number_of_player_home_areas()} players only")
        exit(1)

    if level.number_of_player_home_areas() > 2:
        logger.fatal("This version only supports levels for 2 players")
        exit(1)

    # Hard-code colors for now
    players = [{'name': args.player_names[0], 'color': arcade.color.RED},
               {'name': args.player_names[1], 'color': arcade.color.YELLOW}]

    app = App(level, players)
    app.run()

if __name__ == "__main__":
    main()
