import logging
import os
import time
from typing import Optional, Dict

from arcade import SpriteList, Sound, tilemap
from pytiled_parser.objects import TileMap

from constants import TILE_SCALING, PLAYER_HOME_AREA_LAYER_PREFIX

logger = logging.getLogger(__name__)

class Level:
    def __init__(self, name: str, structure: Dict[str, SpriteList], music: Optional[Sound] = None):
        self.name: str = name
        self.structure: Optional[Dict[str, SpriteList]] = structure
        self._music: Optional[Sound] = music

    def play_music(self, volume: int):
        if self._music:
            self._music.stop()
            time.sleep(0.5)
            self._music.play(volume=volume / 100)

    def stop_music(self):
        logger.debug("Stop music")
        if self._music:
            self._music.stop()

    def loop_music(self, volume: int):
        if self._music:
            position = self._music.get_stream_position()
            if position == 0.0:
                logger.debug("loop music")
                self._music.stop()
                self._music.play(volume=volume / 100)

    def number_of_player_home_areas(self):
        return len([layer for layer in self.structure if PLAYER_HOME_AREA_LAYER_PREFIX in layer])        


    @classmethod
    def from_tmx(cls, tmx_file: str, music_file: str = None):
        tile_map: TileMap = tilemap.read_tmx(tmx_file=tmx_file)

        struc = {}
        music = None

        for layer in tile_map.layers:
            
            struc[layer.name] = tilemap.process_layer(map_object=tile_map,
                                                      layer_name=layer.name,
                                                      scaling=TILE_SCALING,
                                                      use_spatial_hash=True)
        if music_file:
            music = Sound(music_file, streaming=True)

        map_name = os.path.splitext(os.path.basename(tmx_file))[0]

        if tile_map.properties and 'name' in tile_map.properties:
            map_name = tile_map.properties['name'] 

        
            

        return cls(map_name, struc, music)
