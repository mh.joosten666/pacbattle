import logging
from typing import Tuple, Dict, Optional, Callable, List

import arcade
from arcade import Sprite, PhysicsEngineSimple, SpriteList, check_for_collision_with_list, play_sound, Color, Texture
from pymunk import Vec2d

from constants import NUMBER_OF_LIVES, GAME_SOUNDS, GAME_SOUND_VOLUME, PLAYER_SCALE, COIN_SCORE, DO_NOT_TOUCH_DAMAGE, \
    BULLET_HIT_DAMAGE, \
    SELF_BULLET_HIT_DAMAGE, KILL_BONUS
from explosion import Explosion

logger = logging.getLogger(__name__)

class Player(Sprite):
    def __init__(self, name: str, color: Tuple[int, int, int], wall_list: SpriteList, home_area: SpriteList, explosion_list: SpriteList, 
    explosion_textures: List[Texture]):
        super().__init__(filename='assets/player.png', scale=PLAYER_SCALE)
        self._color = color
        self._name: str = name
        self._lives: int = NUMBER_OF_LIVES
        self._number_of_kills = 0
        self._score: int = 0
        self._health: int = 100
        self._last_hit_by: Player = None
        self._direction: Vec2d = Vec2d(1, 0)
        self.physics_engine: PhysicsEngineSimple = PhysicsEngineSimple(self, wall_list)
        self._home_area: SpriteList = home_area
        self._is_home: bool = False
        self._explosion_list = explosion_list
        self._explosion_textures = explosion_textures

        self.on_player_died: Optional[Callable[[Dict], None]] = None
        self._color_home_area(color)
        self._move_to_start_pos()

    @property
    def health(self) -> int:
        return self._health

    @property
    def name(self) -> str:
        return self._name

    @property
    def lives(self) -> int:
        return self._lives

    @health.setter
    def health(self, health: int):
        self._health = health
        self._check_health()

    @property
    def score(self) -> int:
        return self._score

    @score.setter
    def score(self, score: int):
        self._score = score

    @property
    def number_of_kills(self) -> int:
        return self._number_of_kills

    @number_of_kills.setter
    def number_of_kills(self, number_of_kills: int):
        self._number_of_kills = number_of_kills

    def respawn(self):
        logger.info(f"Respawning {self._name}")
        self.stop()
        self._health = 100
        self._last_hit_by = None
        self._direction = Vec2d(1, 0)
        self._move_to_start_pos()

    def _color_home_area(self, color: Color):        
        for sprite in self._home_area:
            sprite.color = color
            sprite.alpha = 60

    def _move_to_start_pos(self):
        self.center_x = self._home_area[0].center_x
        self.center_y = self._home_area[0].center_y

    def move(self, direction: Tuple[float, float], speed: int):
        newX, newY = direction
        self._direction.x = newX
        self._direction.y = newY
        self.angle = self._direction.angle_degrees

        self.change_x = self._direction.x * speed
        self.change_y = self._direction.y * speed

    def _is_destroyed(self) -> bool:
        return self.health <= 0

    def _is_dead(self) -> bool:
        return self._lives == 0

    def _toggle_home(self, new_value: bool):
        if new_value != self._is_home:
            self._is_home = new_value
            if self._is_home and self._health < 100:
                arcade.schedule(self._recover_health, 1)
            else:
                arcade.unschedule(self._recover_health)

    def _recover_health(self, delta_time: float):
        if self._health + 2 >= 100:
            logger.info(f"{self._name} is recovered")
            arcade.unschedule(self._recover_health)
            self._health = 100
            play_sound(GAME_SOUNDS['player_healthy'], volume=GAME_SOUND_VOLUME)
        else:
            self._health += 4
            logger.debug(f"{self._name} is recovering...")

    def check_hit_by_bullet(self, bullet):
        play_sound(GAME_SOUNDS['ouch'], volume=GAME_SOUND_VOLUME)
        if bullet.origin_player == self:
            logger.info(f"Don't shoot yourself!")
            self.health -= SELF_BULLET_HIT_DAMAGE
        else:            
            if self._is_home:
                logger.info(
                    f"{bullet.origin_player.get_player_data()['name']} cowardly hit {self._name} when he/she is home!")
                self.health -= BULLET_HIT_DAMAGE / 2
            else:
                logger.info(f"{bullet.origin_player.get_player_data()['name']} hit {self._name}!")
                self.health -= BULLET_HIT_DAMAGE 

        self._last_hit_by = bullet.origin_player

    def check_target_hit(self, coin_list: SpriteList, do_not_touch_list: SpriteList):
        # Player -> home area
        home_collisions = check_for_collision_with_list(self, self._home_area)
        self._toggle_home(len(home_collisions) > 0)

        # Player -> coin
        coin_collisions = check_for_collision_with_list(self, coin_list)
        for coin in coin_collisions:
            # Remove the coin
            coin.kill()
            play_sound(GAME_SOUNDS['coin_collect'], volume=GAME_SOUND_VOLUME)
            self._score += COIN_SCORE

            # Player -> do-not-touch obstacle
        dnt_collisions = check_for_collision_with_list(self, do_not_touch_list)
        if dnt_collisions:
            play_sound(GAME_SOUNDS['ouch'], volume=GAME_SOUND_VOLUME)
            self.health -= DO_NOT_TOUCH_DAMAGE

    
    def _show_explosion(self):
            explosion = Explosion(self._explosion_textures)
            explosion.center_x = self.center_x
            explosion.center_y = self.center_y
            explosion.update()
            self._explosion_list.append(explosion)    

    def _check_health(self):        
        if self._is_destroyed():
            self._show_explosion()
            play_sound(GAME_SOUNDS['player_destroyed'], volume=GAME_SOUND_VOLUME)                        
            if self._last_hit_by:
                if self._last_hit_by == self:
                    logger.info(f"Congratulations {self._name}! You have destroyed yourself..")
                    self._last_hit_by.score -= 5
                else:
                    logger.info(f"{self._name} was destroyed by {self._last_hit_by.get_player_data()['name']}")
                    self._last_hit_by.score += KILL_BONUS
                    self._last_hit_by.number_of_kills += 1
            else:
                logger.info(f"{self._name} has been destroyed")

            self._lives -= 1
            if self._is_dead():
                logger.info(f"{self._name} is dead")
                play_sound(GAME_SOUNDS['player_dead'], volume=GAME_SOUND_VOLUME)
                if self.on_player_died:
                    self.on_player_died(self.get_player_data())
                self.kill()
            else:
                self.respawn()

    def get_direction(self) -> Vec2d:
        return self._direction

    def get_player_data(self) -> dict:
        return {'name': self._name, 'color': self._color, 'score': self._score, 'health': self.health,
                'lives': self._lives, 'number_of_kills': self._number_of_kills}

    def __str__(self):
        return f"Player: {self._name}\nScore: {self._score}\nHealth: {self.health}"
