from typing import Dict, List

import arcade
from arcade import Color


class Scoreboard:

    def __init__(self, players: Dict[str, Color]):
        self._scores: List[ScoreboardValue] = [ScoreboardValue(player[0], player[1]) for player in players.items()]

    def draw(self, x: int, y: int, columns=None, only_alive_players: bool = True):
        if columns is None:
            columns = ["Score", "Health", "Lives", "Kill count"]

        if only_alive_players:
            scores = [item for item in self._scores if not item.is_dead]
        else:
            scores = [item for item in self._scores]

        header_x = x + 100
        header_y = y

        # Headers
        for column in columns:
            width = 50
            if column == 'Kill count':
                width = 62
            arcade.draw_text(f"{column}", header_x, header_y, arcade.color.WHITE, 12, width=width, anchor_y="center",
                             align="center")
            header_x += 50

        start_y = y - 20
        name_x = x + 20

        for entry in scores:
            start_x = x + 100

            score_entry: ScoreboardValue = entry
            arcade.draw_circle_filled(center_x=x, center_y=start_y, radius=5, color=score_entry.color)
            arcade.draw_text(f"{score_entry.player_name}", name_x, start_y, arcade.color.WHITE, 15, width=80,
                             anchor_y="center",
                             align="center")

            for column in columns:
                if column == "Score":
                    arcade.draw_text(f"{score_entry.score}", start_x, start_y, arcade.color.WHITE, 15, width=50,
                                     anchor_y="center",
                                     align="center")
                if column == "Health":
                    health_color = arcade.color.RED if score_entry.health < 30 else arcade.color.YELLOW if score_entry.health < 50 else arcade.color.GREEN_YELLOW
                    arcade.draw_xywh_rectangle_filled(start_x + 5, start_y - 6, 0.01 * score_entry.health * 45, 15,
                                                      health_color)
                if column == "Lives":
                    lives_color = arcade.color.RED if score_entry.lives < 2 else arcade.color.WHITE
                    arcade.draw_text(f"{score_entry.lives}", start_x, start_y, lives_color, 15, width=50,
                                     anchor_y="center",
                                     align="center")
                if column == "Kill count":
                    arcade.draw_text(f"{score_entry.kill_count}", start_x, start_y, arcade.color.WHITE, 15, width=50,
                                     anchor_y="center", align="center")

                start_x += 50

            start_y -= 20

    def update(self, player_name: str, score: int, health: int, lives: int, kill_count: int):

        for idx in range(0, len(self._scores)):
            if self._scores[idx].player_name == player_name:
                self._scores[idx].score = score
                self._scores[idx].health = health
                self._scores[idx].lives = lives
                self._scores[idx].kill_count = kill_count

        self._scores.sort(key=lambda item: (item.score, item.lives, item.kill_count),
                          reverse=True)

    def mark_as_dead(self, player_name):
        player_item = [item for item in self._scores if item.player_name == player_name]
        if player_item:
            player_item[0].is_dead = True

    def get_winner(self) -> str:
        if self._all_the_same(self._scores):
            return None

        return self._scores[0].player_name

    def _all_the_same(self, elements):
        if not elements:
            return True
        return [elements[0]] * len(elements) == elements

class ScoreboardValue:
    def __init__(self, player_name: str, color: Color):
        self._player_name: str = player_name
        self._color: Color = color
        self._score: int = 0
        self._health: int = 0
        self._lives: int = 0
        self._kill_count: int = 0
        self._is_dead = False

    @property
    def player_name(self) -> str:
        return self._player_name

    @property
    def color(self) -> Color:
        return self._color

    @property
    def score(self) -> int:
        return self._score

    @score.setter
    def score(self, score: int):
        self._score = score

    @property
    def health(self) -> int:
        return self._health

    @health.setter
    def health(self, health: int):
        self._health = health

    @property
    def kill_count(self) -> int:
        return self._kill_count

    @kill_count.setter
    def kill_count(self, kill_count: int):
        self._kill_count = kill_count

    @property
    def lives(self) -> int:
        return self._lives

    @lives.setter
    def lives(self, lives: int):
        self._lives = lives

    @property
    def is_dead(self) -> bool:
        return self._is_dead

    @is_dead.setter
    def is_dead(self, is_dead: bool):
        self._is_dead = is_dead

    def __eq__(self, other):
        return self._score == other.score and self._lives == other.lives and self._kill_count == other.kill_count
