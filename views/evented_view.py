from typing import Callable, Optional

from arcade import View


class EventedView(View):

    def __init__(self):
        super().__init__()
        self.on_key_pressed: Optional[Callable[[int, int], None]] = None

    def on_key_press(self, key, modifiers):
        if self.on_key_pressed:
            self.on_key_pressed(key, modifiers)