from typing import Optional, List, Dict, Callable

import arcade
from arcade import View, set_background_color, csscolor, start_render, SpriteList, Texture
from numpy.random.mtrand import randint

from bullet import Bullet
from constants import SCREEN_HEIGHT, SCREEN_WIDTH, WINDOW_HEIGHT, NORTH, PLAYER_SPEED, EAST, \
    SOUTH, WEST, COIN_SCALING, WALLS, DO_NOT_TOUCH_OBJECTS, \
    MUSIC_VOLUME, WINDOW_WIDTH, PLAYER_HOME_AREA_LAYER_PREFIX
from level import Level
from player import Player
from scoreboard import Scoreboard


class GameView(View):
    """
    Main game view
    """

    def __init__(self, level: Level, players: List[Dict]):
        # Call the parent class and set up the window
        super().__init__()
        self.player_list: Optional[SpriteList] = None
        self.coin_list: Optional[SpriteList] = None
        self.bullet_list: Optional[SpriteList] = None
        self.explosions_list: Optional[SpriteList] = None
        self._scoreboard: Optional[Scoreboard] = None
        self.level: Optional[Level] = None
        self._is_ended: bool = False

        self.on_game_ended: Optional[Callable[[Scoreboard], None]] = None
        self.explosion_texture_list = self._load_explosion_textures()

        self.level: Level = level
        self.players: List[Dict] = players

    def _load_explosion_textures(self) -> List[Texture]:
        columns = 16
        count = 60
        sprite_width = 256
        sprite_height = 256
        file_name = ":resources:images/spritesheets/explosion.png"

        # Load the explosions from a sprite sheet
        return arcade.load_spritesheet(file_name, sprite_width, sprite_height, columns, count)



    def setup(self):
        """ Set up the game here. Call this function to restart the game. """
        set_background_color(csscolor.BLACK)
        self._is_ended = False
        self._scoreboard = []
        arcade.unschedule(self._add_coin)
        arcade.unschedule(self._remove_coin)
        if self.level:
            self.level.stop_music()

        self._setup_players(self.players)
        self._setup_coins()
        self.bullet_list = SpriteList()
        self.level.play_music(MUSIC_VOLUME)
        

    def _setup_players(self, players: List[Dict]):
        if len(players) > 2:
            raise ValueError("A local game supports 2 players only")
        
        self._scoreboard = Scoreboard({player['name']: player['color'] for player in self.players})        
        self.player_list = SpriteList()
        self.explosions_list = SpriteList()            

        for idx, player in enumerate(players):
            home_area = self.level.structure[PLAYER_HOME_AREA_LAYER_PREFIX + f"_{idx + 1}"]
            player: Player = Player(name=player['name'], color=player['color'],
                                    wall_list=self.level.structure[WALLS], home_area=home_area, explosion_list=self.explosions_list, explosion_textures=self.explosion_texture_list)
            player.on_player_died = self._handle_player_died
            self.player_list.append(player)

        

    def _setup_coins(self):
        self.coin_list = SpriteList(use_spatial_hash=True, is_static=True)
        arcade.schedule(self._add_coin, 5)
        arcade.schedule(self._remove_coin, 10)

    def _add_coin(self, delta_time: float):
        coin = arcade.Sprite(":resources:images/items/coinGold.png", COIN_SCALING)
        posX = randint(10, SCREEN_WIDTH - 10)
        posY = randint(10, SCREEN_HEIGHT - 10)
        coin.set_position(posX, posY)
        self.coin_list.append(coin)

    def _remove_coin(self, delta_time: float):
        if len(self.coin_list) > 0:
            if len(self.coin_list) == 1:
                self.coin_list.pop().kill()
            else:
                index = randint(0, len(self.coin_list) - 1)
                self.coin_list.pop(index).kill()

    def _fire_bullet(self, player: Player):
        bullet = Bullet()
        bullet.fire(player)
        self.bullet_list.append(bullet)

    def _draw_title(self):
        start_y = WINDOW_HEIGHT - 40
        start_x = WINDOW_WIDTH / 2 + 30

        arcade.draw_text(f"{self.level.name}", start_x=start_x, start_y=start_y, color=arcade.color.WHITE, font_size=25,
                         anchor_y="center", anchor_x="center",
                         align="center")

    def _draw_scoreboard(self):
        self._scoreboard.draw(x=20, y=WINDOW_HEIGHT - 10)

    def _draw_level(self):
        for layer in self.level.structure.values():
            layer.draw()

    def on_draw(self):
        """ Render the screen. """
        start_render()
        # Code to draw the screen goes here
        self._draw_level()
        self.player_list.draw()
        self.coin_list.draw()
        self.bullet_list.draw()
        self.explosions_list.draw()
        self._draw_title()
        self._draw_scoreboard()

    def on_update(self, delta_time: float):
        self._update_players()
        self._update_bullets()
        self.explosions_list.update()

        if not self._is_ended:
            self.level.loop_music(volume=MUSIC_VOLUME)
        else:
            self.level.stop_music()

    def _update_bullets(self):
        self.bullet_list.update()
        for bullet in self.bullet_list:
            b: Bullet = bullet
            b.check_target_hit(wall_list=self.level.structure[WALLS],
                               do_not_touch_list=self.level.structure[DO_NOT_TOUCH_OBJECTS],
                               player_list=self.player_list)

    def _update_players(self):
        if len(self.player_list) == 1:
            last_player_standing: Player = self.player_list[0]
            last_player_standing.kill()
            self._end_game()

        for player in self.player_list:
            p: Player = player
            p.physics_engine.update()
            p.check_target_hit(coin_list=self.coin_list, do_not_touch_list=self.level.structure[DO_NOT_TOUCH_OBJECTS])
            self._scoreboard.update(p.name, p.score, p.health, p.lives, p.number_of_kills)

    def _handle_player_died(self, player_data: Dict):
        self._scoreboard.mark_as_dead(player_data['name'])

    def _end_game(self):
        self._is_ended = True
        self.level.stop_music()
        if self.on_game_ended:
            self.on_game_ended(self._scoreboard)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.Q:
            self._end_game()

        if key == arcade.key.UP:
            player1: Player = self.player_list[0]
            player1.move(NORTH, PLAYER_SPEED)
        elif key == arcade.key.RIGHT:
            player1: Player = self.player_list[0]
            player1.move(EAST, PLAYER_SPEED)
        elif key == arcade.key.DOWN:
            player1: Player = self.player_list[0]
            player1.move(SOUTH, PLAYER_SPEED)
        elif key == arcade.key.LEFT:
            player1: Player = self.player_list[0]
            player1.move(WEST, PLAYER_SPEED)
        elif key == arcade.key.ENTER:
            player1: Player = self.player_list[0]
            player1.stop()
        elif key == arcade.key.SPACE:
            player1: Player = self.player_list[0]
            self._fire_bullet(player1)

        if key == arcade.key.W:
            player1: Player = self.player_list[1]
            player1.move(NORTH, PLAYER_SPEED)
        elif key == arcade.key.D:
            player1: Player = self.player_list[1]
            player1.move(EAST, PLAYER_SPEED)
        elif key == arcade.key.X:
            player1: Player = self.player_list[1]
            player1.move(SOUTH, PLAYER_SPEED)
        elif key == arcade.key.A:
            player1: Player = self.player_list[1]
            player1.move(WEST, PLAYER_SPEED)
        elif key == arcade.key.S:
            player1: Player = self.player_list[1]
            player1.stop()
        elif key == arcade.key.C:
            player1: Player = self.player_list[1]
            self._fire_bullet(player1)
