import arcade

from constants import SCREEN_WIDTH, SCREEN_HEIGHT, DO_NOT_TOUCH_OBJECTS, DO_NOT_TOUCH_DAMAGE, COIN_SCORE, \
    BULLET_HIT_DAMAGE, KILL_BONUS
from level import Level
from views.evented_view import EventedView


class IntroView(EventedView):

    def __init__(self, players, level: Level):
        super().__init__()
        self._players = players
        self._level: Level = level
        self._instruction_text = "Move your pacman around and eat as many coins possible to gain the highest possible score.\nBut beware of your opponents," \
                                 "they can shoot at you and kill you, but you can kill them as well.\nThe game is over when one player " \
                                 "is left.\nEach player has its own 'home' area in which he/she can replenish health after hit by bullets."
        self._sprites = {"player": arcade.Sprite("assets/player.png").texture,
                         "coin": arcade.Sprite(":resources:images/items/coinGold.png").texture,
                         "dnt": self._level.structure[DO_NOT_TOUCH_OBJECTS][0].texture,
                         "bullet": arcade.Sprite(":resources:images/space_shooter/laserBlue01.png").texture}

    def on_show(self):
        arcade.set_background_color(arcade.csscolor.DARK_SLATE_BLUE)

    def on_draw(self):
        """ Draw this view """
        arcade.start_render()

        arcade.draw_text("Welcome to PacBattle", SCREEN_WIDTH / 2, SCREEN_HEIGHT,
                         arcade.color.WHITE, font_size=50, anchor_x="center")

        self._draw_game_info(x=20, y=SCREEN_HEIGHT - 50)
        self._draw_instructions(x=20, y=SCREEN_HEIGHT - 190, text=self._instruction_text)
        self._draw_rewards_damages(x=20, y=SCREEN_HEIGHT - 340)
        self._draw_controls(x=SCREEN_WIDTH - 375, y=SCREEN_HEIGHT - 340)

        arcade.draw_text("Press any key to start", SCREEN_WIDTH / 2, 20,
                         arcade.color.WHITE, font_size=20, anchor_x="center")

    def _draw_game_info(self, x, y):
        selected_level_label_width = 160
        selected_level_x = x + selected_level_label_width
        arcade.draw_text("Game info:", x, y,
                         arcade.color.WHITE, font_size=20, align="left")
        arcade.draw_text("Selected level:", x, y - 10,
                         arcade.color.WHITE, font_size=15, width=selected_level_label_width, align="left",
                         anchor_y="top")
        arcade.draw_text(f"{self._level.name}", selected_level_x - 5, y - 10,
                         arcade.color.YELLOW, font_size=15, align="left", anchor_y="top")

        player_start_x = selected_level_x
        player_start_y = y - 50
        arcade.draw_text("Players:", x, player_start_y,
                         arcade.color.WHITE, font_size=15, width=selected_level_label_width, align="left",
                         anchor_y="center")

        for player in self._players:
            arcade.draw_circle_filled(center_x=player_start_x, center_y=player_start_y, radius=5, color=player['color'])
            arcade.draw_text(f"{player['name']}", player_start_x + 20, player_start_y, arcade.color.WHITE, 12, width=80,
                             anchor_y="center",
                             align="left")
            player_start_y -= 20

    def _draw_rewards_damages(self, x, y):
        arcade.draw_text(f"Rewards/damages:", x, y,
                         arcade.color.WHITE, font_size=20, anchor_x="left")

        start_y = y - 30
        start_x = x + 10

        arcade.draw_texture_rectangle(start_x, start_y, 30, 30, self._sprites["dnt"])
        arcade.draw_texture_rectangle(start_x, start_y - 40, 40, 40, self._sprites["coin"])
        arcade.draw_texture_rectangle(start_x, start_y - 80, 30, 30, self._sprites["player"])
        arcade.draw_texture_rectangle(start_x + 30, start_y - 80, 30, 30, self._sprites["bullet"])
        arcade.draw_texture_rectangle(start_x + 60, start_y - 80, 30, 30, self._sprites["player"])

        arcade.draw_text(f"Health -{DO_NOT_TOUCH_DAMAGE}%", start_x + 100, start_y,
                         arcade.color.WHITE, font_size=15, align="left", anchor_y="center")
        arcade.draw_text(f"Score +{COIN_SCORE} pt", start_x + 100, start_y - 40,
                         arcade.color.WHITE, font_size=15, align="left", anchor_y="center")
        arcade.draw_text(f"Direct hit: health -{BULLET_HIT_DAMAGE}% (-{BULLET_HIT_DAMAGE // 2}% when in home area)",
                         start_x + 100, start_y - 80,
                         arcade.color.WHITE, font_size=15, align="left", anchor_y="center")
        arcade.draw_text(f"Kill: score +{KILL_BONUS} pts for the attacking player", start_x + 100, start_y - 100,
                         arcade.color.WHITE, font_size=15, align="left", anchor_y="center")

    def _draw_instructions(self, x: int, y: int, text: str):
        arcade.draw_text(f"How to play:", x, y,
                         arcade.color.WHITE, font_size=20, anchor_x="left")
        arcade.draw_text(text, x, y - 10,
                         arcade.color.WHITE, font_size=12, anchor_x="left", anchor_y="top")

    def _draw_controls(self, x: int, y: int):
        arcade.draw_text(f"Controls:", x, y,
                         arcade.color.WHITE, font_size=20, anchor_x="left")

        key_p1_x = x
        key_starty = y - 15
        key_description_x = x + 75
        key_description_width = 120
        key_p2_x = key_description_x + key_description_width

        arcade.draw_circle_filled(center_x=key_p1_x + 10, center_y=key_starty, radius=5,
                                  color=self._players[0]["color"])
        arcade.draw_circle_filled(center_x=key_p2_x + 10, center_y=key_starty, radius=5,
                                  color=self._players[1]["color"])

        arcade.draw_text("Move up", key_description_x, key_starty - 25, arcade.color.WHITE, 15,
                         width=key_description_width,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("Move down", key_description_x, key_starty - 50, arcade.color.WHITE, 15,
                         width=key_description_width,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("Move left", key_description_x, key_starty - 75, arcade.color.WHITE, 15,
                         width=key_description_width,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("Move right", key_description_x, key_starty - 100, arcade.color.WHITE, 15,
                         width=key_description_width,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("Stop moving", key_description_x, key_starty - 125, arcade.color.WHITE, 15,
                         width=key_description_width,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("Fire a bullet", key_description_x, key_starty - 150, arcade.color.WHITE, 15,
                         width=key_description_width,
                         anchor_y="center",
                         align="left")

        # Player 1

        arcade.draw_text("UP", key_p1_x, key_starty - 25, arcade.color.WHITE, 15, width=80,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("DOWN", key_p1_x, key_starty - 50, arcade.color.WHITE, 15, width=80,
                         anchor_y="center",
                         align="left")

        arcade.draw_text("LEFT", key_p1_x, key_starty - 75, arcade.color.WHITE, 15, width=80,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("RIGHT", key_p1_x, key_starty - 100, arcade.color.WHITE, 15, width=80,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("ENTER", key_p1_x, key_starty - 125, arcade.color.WHITE, 15, width=80,
                         anchor_y="center",
                         align="left")
        arcade.draw_text("SPACE", key_p1_x, key_starty - 150, arcade.color.WHITE, 15, width=80,
                         anchor_y="center",
                         align="left")

        # Player 2

        arcade.draw_text("W", key_p2_x, key_starty - 25, arcade.color.WHITE, 15, width=20,
                         anchor_y="center",
                         align="center")
        arcade.draw_text("X", key_p2_x, key_starty - 50, arcade.color.WHITE, 15, width=20,
                         anchor_y="center",
                         align="center")

        arcade.draw_text("A", key_p2_x, key_starty - 75, arcade.color.WHITE, 15, width=20,
                         anchor_y="center",
                         align="center")
        arcade.draw_text("D", key_p2_x, key_starty - 100, arcade.color.WHITE, 15, width=20,
                         anchor_y="center",
                         align="center")
        arcade.draw_text("S", key_p2_x, key_starty - 125, arcade.color.WHITE, 15, width=20,
                         anchor_y="center",
                         align="center")
        arcade.draw_text("C", key_p2_x, key_starty - 150, arcade.color.WHITE, 15, width=20,
                         anchor_y="center",
                         align="center")
