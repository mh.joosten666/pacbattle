import arcade

from constants import SCREEN_WIDTH, SCREEN_HEIGHT
from scoreboard import Scoreboard
from views.evented_view import EventedView


class ScoreView(EventedView):

    def __init__(self, scoreboard: Scoreboard):
        super().__init__()
        self._scoreboard: Scoreboard = scoreboard
        self._winner = scoreboard.get_winner()

    def on_show(self):
        arcade.set_background_color(arcade.csscolor.DARK_SLATE_BLUE)

    def on_draw(self):
        """ Draw this view """
        arcade.start_render()

        arcade.draw_text("Game ended!", SCREEN_WIDTH / 2, SCREEN_HEIGHT,
                         arcade.color.WHITE, font_size=50, anchor_x="center")
        arcade.draw_text(f"{self._winner} is the winner" if self._winner else "It's a tie!", SCREEN_WIDTH / 2,
                         SCREEN_HEIGHT - 85,
                         arcade.color.WHITE, font_size=50, anchor_x="center")

        arcade.draw_text(f"Final score:", SCREEN_WIDTH / 2,
                         SCREEN_HEIGHT - 150,
                         arcade.color.WHITE, font_size=20, anchor_x="center")

        self._scoreboard.draw(x=SCREEN_WIDTH / 2 - 60, y=SCREEN_HEIGHT - 170, columns=["Score", "Kill count"],
                              only_alive_players=False)

        arcade.draw_text("Press space to start over or Q for quit", SCREEN_WIDTH / 2, 20,
                         arcade.color.WHITE, font_size=20, anchor_x="center")
